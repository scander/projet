#ifndef MONSTRE_T_HPP
#define MONSTRE_T_HPP

#include <thoth/window.hpp>

#include "direction_t.hpp"


// Monstre
class monstre_t
{
public:

    friend class level_t;

private:
	
	// Sprite
	thoth::sprite_animated m_sprite;

    // Direction
    direction_t m_direction;

    // Temps changement direction
    float m_time_direction;

    std::string monstre_name;


	
public:
	
	// Constructeur par défaut
	monstre_t() = default;
	
	// Constructeur
    monstre_t(std::string const & monstre_name, hopp::vector2<float> const & position) :

        m_sprite(),
        m_direction(direction_t::center),
        m_time_direction(0.f),
        monstre_name(monstre_name)
{

		if (monstre_name == "marron")
		{
			m_sprite = thoth::textures()["monstre_marron_en_vie_gauche_1"];
			
			m_sprite.add
			(
				"en_vie_gauche", 0.100f,
				{ thoth::textures()["monstre_marron_en_vie_gauche_1"], thoth::textures()["monstre_marron_en_vie_gauche_2"] }
			);
			m_sprite.add
			(
				"en_vie_droite", 0.100f,
				{ thoth::textures()["monstre_marron_en_vie_droite_1"], thoth::textures()["monstre_marron_en_vie_droite_2"] }
			);
			
			m_sprite.animate("en_vie_gauche");
		}
		else if (monstre_name == "tronc")
		{
			m_sprite = thoth::textures()["monstre_tronc"];
		}
        else if (monstre_name == "plante")
        {

            m_sprite = thoth::textures()["monstre_plante_en_vie1"];

            m_sprite.add
            (

                "en_vie", 0.100f,
                { thoth::textures()["monstre_plante_en_vie1"], thoth::textures()["monstre_plante_en_vie2"] }
            );
           //m_sprite.animate("monstre_plante_en_vie2");

        }
		else
		{
			throw std::invalid_argument("monstre_t: monstre_name « " + monstre_name + " » inconnu");
		}
		
		m_sprite.set_position(position);


	}
	
	// Sprite
	thoth::sprite_animated const & sprite() const { return m_sprite; }
	
	// Sprite
	thoth::sprite_animated & sprite() { return m_sprite; }
	
	// Update
	void update(float const time_elapsed) { m_sprite.update(time_elapsed); }

    // Move
    void move(float const moving)
    {
        if (m_direction == direction_t::left)
        {
            m_sprite.move(-moving, 0.f);
        }
        else if (m_direction == direction_t::right)
        {
            m_sprite.move(moving, 0.f);
        }
        else if (m_direction == direction_t::down)
        {
            m_sprite.move(0.f, moving);
        }
    }
};

// Affichage dans une fenêtre
inline thoth::window & operator <<(thoth::window & window, monstre_t const & monstre)
{
	window << monstre.sprite();
	return window;
}

#endif
