﻿// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>
#include <string>
#include <vector>

#include <hopp/geometry.hpp>

#include <thoth/window.hpp>

#include "level_t.hpp"
#include "textures.hpp"


int main()
{
	// Chargement textures
	chargement_textures();
	
	// Window
	std::string title_base = "no_name_yet";
	thoth::window window({ 1024, 768 }, title_base);
    window.camera().zoom(3.f);
	
	// Level
	level_t level("../level/01.txt");
	
	while (window.is_open())
	{
		// Time elapsed
		auto const time_elapsed = float(window.time_elapsed());
		
		// Event
		while (thoth::event const event = window.event())
		{
			// Window
			if (event.window_closed()) { window.close(); }
			else if (auto const size = event.window_resized())
			{
                std::cout << "Window resized " << *size << std::endl;
			}
			// Mouse wheel
			else if (auto const delta = event.mouse_wheel_moved())
			{
				window.camera().zoom(float(std::abs(*delta)) + float(*delta) * 0.1f);
			}
		}
		
		// Evolution du level
		level.evolution(time_elapsed);
		level.update(time_elapsed);
		
		// Camera
		window.camera().set_center(level.player().sprite().position());
		window.camera().set_center
		(
			window.camera().center().x,
			std::min(window.camera().center().y, 930.f)
		);
		
		// Clear
		window.clear(/*hopp::color(3u, 205u, 238u)*/);
		
		window << level;
		
		// Update
		window.update();
		
		// Title
		window.set_title(hopp::to_string
		(
			title_base,
			// Mouse position
			" | ", "Position de la souris dans le monde = ", thoth::mouse::position_in_camera(window.camera()),
			" | ", "dans la fenetre = ", thoth::mouse::position_in_window(window),
			// FPS
			" | ", "FPS = ", hopp::uint(window.fps())
		));
	}


	
	return 0;
}

