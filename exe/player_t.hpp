#ifndef PLAYER_T_HPP
#define PLAYER_T_HPP

#include <stdexcept>

#include <thoth/window.hpp>


// Player
class player_t
{
public:

    friend class level_t;

private:
	
	// Sprite
	thoth::sprite_animated m_sprite;

    // Temps du saut
    float m_time_jump;
    float m_time_frapper;
    float m_time_touche;


	
public:
	
	// Constructeur par défaut
	player_t() = default;
	
	// Constructeur
    player_t(std::string const & player_name, hopp::vector2<float> const & position) :

        m_time_jump(0.f),
        m_time_frapper(0.f),
        m_time_touche(0.f)


{
		if (player_name == "green")
		{
			m_sprite = thoth::textures()["player_green_en_vie_droite_1"];
			
			m_sprite.add
			(
				"en_vie_droite", 0.100f,
				{ thoth::textures()["player_green_en_vie_droite_1"], thoth::textures()["player_green_en_vie_droite_2"] }
			);
			m_sprite.add
			(
				"en_vie_gauche", 0.100f,
				{ thoth::textures()["player_green_en_vie_gauche_1"], thoth::textures()["player_green_en_vie_gauche_2"] }
			);
			m_sprite.add
			(
				"saut_gauche", 0.100f,
				{ thoth::textures()["player_green_saut_gauche"] }
			);
			m_sprite.add
			(
				"saut_droite", 0.100f,
				{ thoth::textures()["player_green_saut_droite"] }
			);
			m_sprite.add
			(
				"bas_gauche", 0.100f,
				{ thoth::textures()["player_green_bas_gauche"] }
			);
			m_sprite.add
			(
				"bas_droite", 0.100f,
				{ thoth::textures()["player_green_bas_droite"] }
			);
			m_sprite.add
			(
				"gauche", 0.100f,
				{ thoth::textures()["player_green_gauche_1"], thoth::textures()["player_green_gauche_2"], thoth::textures()["player_green_gauche_3"], thoth::textures()["player_green_gauche_4"] }
			);
			m_sprite.add
			(
				"droite", 0.100f,
				{ thoth::textures()["player_green_droite_1"], thoth::textures()["player_green_droite_2"], thoth::textures()["player_green_droite_3"], thoth::textures()["player_green_droite_4"] }
			);
			m_sprite.add
			(
                "touché_gauche", 0.100f,
				{ thoth::textures()["player_green_touché_gauche_1"], thoth::textures()["player_green_touché_gauche_2"] }
			);
			m_sprite.add
			(
                "touché_droite", 0.100f,
				{ thoth::textures()["player_green_touché_droite_1"], thoth::textures()["player_green_touché_droite_2"] }
			);
            m_sprite.add
            (
                "mort_droite", 0.100f,
                { thoth::textures()["player_green_mort1_droite"], thoth::textures()["player_green_mort2_droite"], thoth::textures()["player_green_mort3_droite"] }
            );
            m_sprite.add
            (
                "mort_gauche", 0.100f,
                { thoth::textures()["player_green_mort1_gauche"],thoth::textures()["player_green_mort1_gauche"], thoth::textures()["player_green_mort2_gauche"], thoth::textures()["player_green_mort3_gauche"] }
            );
            m_sprite.add
            (
                "frapper_droite", 0.100f,
                { thoth::textures()["player_green_frapper_droite"]}
            );
            m_sprite.add
            (
                "frapper_gauche", 0.100f,
                { thoth::textures()["player_green_frapper_gauche"]}
            );
		}
		else
		{
			throw std::invalid_argument("player_t: player_name « " + player_name + " » inconnu");
		}
		
		m_sprite.set_position(position);
		m_sprite.animate("en_vie_droite");
	}
	
	// Sprite
	thoth::sprite_animated const & sprite() const { return m_sprite; }
	
	// Sprite
	thoth::sprite_animated & sprite() { return m_sprite; }
	
	// Update
	void update(float const time_elapsed) { m_sprite.update(time_elapsed); }
};

// Affichage dans une fenêtre
inline thoth::window & operator <<(thoth::window & window, player_t const & player)
{
	window << player.sprite();
	return window;
}

#endif
