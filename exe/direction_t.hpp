#ifndef DIRECTION_T_HPP
#define DIRECTION_T_HPP

enum class direction_t { center, up, down, left, right, up_left, up_right, down_left, down_right };

#endif
