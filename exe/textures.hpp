#ifndef TEXTURES_HPP
#define TEXTURES_HPP

#include <thoth/window.hpp>


// Charge toutes les textures
inline void chargement_textures()
{
	// TODO Faire une boucle avec hopp::filesytem
	
	// Décors
	thoth::textures().load_extern("décor_ciel", "../img/décor/ciel.png");
	thoth::textures().load_extern("décor_nuage", "../img/décor/nuage.png");
	
	// Monstres
	thoth::textures().load_extern("monstre_marron_en_vie_droite_1", "../img/monstre/marron_en_vie_droite_1.png");
	thoth::textures().load_extern("monstre_marron_en_vie_droite_2", "../img/monstre/marron_en_vie_droite_2.png");
	thoth::textures().load_extern("monstre_marron_en_vie_gauche_1", "../img/monstre/marron_en_vie_gauche_1.png");
	thoth::textures().load_extern("monstre_marron_en_vie_gauche_2", "../img/monstre/marron_en_vie_gauche_2.png");
	thoth::textures().load_extern("monstre_tronc", "../img/monstre/tronc.png");
    thoth::textures().load_extern("monstre_plante_en_vie1", "../img/monstre/plante_en_vie1.png");
    thoth::textures().load_extern("monstre_plante_en_vie2", "../img/monstre/plante_en_vie2.png");
	
	// Plateformes
    thoth::textures().load_extern("plateforme_sol", "../img/plateforme/sol.png");
    thoth::textures().load_extern("plateforme_herbe", "../img/plateforme/herbe.png");

	
	// Player
	thoth::textures().load_extern("player_green_bas_droite", "../img/player/green_bas_droite.png");
	thoth::textures().load_extern("player_green_bas_gauche", "../img/player/green_bas_gauche.png");
	thoth::textures().load_extern("player_green_droite_1", "../img/player/green_droite_1.png");
	thoth::textures().load_extern("player_green_droite_2", "../img/player/green_droite_2.png");
	thoth::textures().load_extern("player_green_droite_3", "../img/player/green_droite_3.png");
	thoth::textures().load_extern("player_green_droite_4", "../img/player/green_droite_4.png");
	thoth::textures().load_extern("player_green_en_vie_droite_1", "../img/player/green_en_vie_droite_1.png");
	thoth::textures().load_extern("player_green_en_vie_droite_2", "../img/player/green_en_vie_droite_2.png");
	thoth::textures().load_extern("player_green_en_vie_gauche_1", "../img/player/green_en_vie_gauche_1.png");
	thoth::textures().load_extern("player_green_en_vie_gauche_2", "../img/player/green_en_vie_gauche_2.png");
	thoth::textures().load_extern("player_green_gauche_1", "../img/player/green_gauche_1.png");
	thoth::textures().load_extern("player_green_gauche_2", "../img/player/green_gauche_2.png");
	thoth::textures().load_extern("player_green_gauche_3", "../img/player/green_gauche_3.png");
	thoth::textures().load_extern("player_green_gauche_4", "../img/player/green_gauche_4.png");
	thoth::textures().load_extern("player_green_saut_droite", "../img/player/green_saut_droite.png");
	thoth::textures().load_extern("player_green_saut_gauche", "../img/player/green_saut_gauche.png");
	thoth::textures().load_extern("player_green_touché_droite_1", "../img/player/green_touché_droite_1.png");
	thoth::textures().load_extern("player_green_touché_droite_2", "../img/player/green_touché_droite_2.png");
	thoth::textures().load_extern("player_green_touché_gauche_1", "../img/player/green_touché_gauche_1.png");
	thoth::textures().load_extern("player_green_touché_gauche_2", "../img/player/green_touché_gauche_2.png");
    thoth::textures().load_extern("player_green_mort1_droite", "../img/player/green_mort1_droite.png");
    thoth::textures().load_extern("player_green_mort2_droite", "../img/player/green_mort2_droite.png");
    thoth::textures().load_extern("player_green_mort3_droite", "../img/player/green_mort3_droite.png");
    thoth::textures().load_extern("player_green_mort1_gauche", "../img/player/green_mort1_gauche.png");
    thoth::textures().load_extern("player_green_mort2_gauche", "../img/player/green_mort2_gauche.png");
    thoth::textures().load_extern("player_green_mort3_gauche", "../img/player/green_mort3_gauche.png");
    thoth::textures().load_extern("player_green_frapper_droite", "../img/player/green_frapper_droite.png");
    thoth::textures().load_extern("player_green_frapper_gauche", "../img/player/green_frapper_gauche.png");


}


#endif
