#ifndef LEVEL_T_HPP
#define LEVEL_T_HPP

#include <vector>
#include <fstream>
#include <string>

#include <hopp/random.hpp>

#include <thoth/window.hpp>

#include "player_t.hpp"
#include "monstre_t.hpp"


// Level
class level_t
{
private:
	
	// Player
	player_t m_player;
	
	// Plateforme
	std::vector<thoth::sprite> m_plateformes;
	
	// Décors
	std::vector<thoth::sprite> m_decors;
	
	// Monstres
	std::vector<monstre_t> m_monstres;
	
	// Vitesse du jeu
	float m_speed;
	
public:
	
	// Constructeur par défaut
	level_t() :
		m_player(),
		m_plateformes(),
		m_decors(),
		m_monstres(),
		m_speed(250.f)
	{ }
	
	// Constructeur
	// Charge le level depuis un fichier
	level_t(std::string const & nom_fichier) :
		level_t()
	{
		std::ifstream fichier(nom_fichier);
		
		while (fichier)
		{
			// Commentaire & ligne vide & end of file
			if (fichier.peek() == '#' || fichier.peek() == '\n' || fichier.eof())
			{
				std::string ligne;
				std::getline(fichier, ligne);
				continue;
			}
			
			// Type de ligne
			std::string type;
			fichier >> type;
			
			std::string name;
			fichier >> name;
			
			float position_x;
			float position_y;
			fichier >> position_x;
			fichier >> position_y;
			
			std::string ligne;
			std::getline(fichier, ligne);
			
			// Player
			if (type == "player")
			{
				m_player = player_t(name, { position_x, position_y });
			}
			// Plateforme
			else if (type == "plateforme")
			{
				m_plateformes.push_back
				(
					thoth::sprite(thoth::textures()["plateforme_" + name], { position_x, position_y })
				);
			}
			// Décor
			else if (type == "decor")
			{
				m_decors.push_back
				(
					thoth::sprite(thoth::textures()["décor_" + name], { position_x, position_y })
				);
			}
			// Monstre
			else if (type == "monstre")
			{
				m_monstres.push_back
				(
					monstre_t(name, { position_x, position_y })
				);
			}
			else
			{
				throw std::invalid_argument("level_t: type « " + type + " » inconnu");
			}
		}
	}
	
	// Player
	player_t const & player() const { return m_player; }
	
	// Plateformes
	std::vector<thoth::sprite> const & plateformes() const { return m_plateformes; }
	
	// Décors
	std::vector<thoth::sprite> const & decors() const { return m_decors; }
	
	// Décors
	std::vector<monstre_t> const & monstres() const { return m_monstres; }
	
	// Évolution
	void evolution(float const time_elapsed)
	{
		float const moving = m_speed * time_elapsed;
		
        // Déplacement automatique vers le bas
        bool player_down = true;
        bool player_touche = false;
        bool player_frappe = false;


        {

            auto const position_ex = m_player.sprite().position();

            m_player.sprite().move(0.f, moving);

            for(auto const & plateforme : m_plateformes)
            {
                if (hopp::geometry::overlap(m_player.sprite().bounds_global(), plateforme.bounds_global()))
                {
                    m_player.sprite().set_position(position_ex);
                    player_down = false;
                    break;
                }
            }

            for(auto const & monstre : m_monstres)
            {
                if (hopp::geometry::overlap(m_player.sprite().bounds_global(), monstre.sprite().bounds_global()))
                {

                    m_player.sprite().set_position(position_ex);
                    player_down = false;
                    break;
                }
            }
        }
		
		// Autres déplacements
		{
            m_player.m_time_jump -= time_elapsed;
            m_player.m_time_frapper -= time_elapsed;
            m_player.m_time_touche -= time_elapsed;



			auto const position_ex = m_player.sprite().position();

            if (player_down == false && thoth::key_pressed(thoth::keyboard::up))
            {
                m_player.m_time_jump = 0.9f;

            }
            if(player_touche==false){

                if (thoth::key_pressed(thoth::keyboard::left))
                {
                    m_player.sprite().animate("gauche");
                    m_player.sprite().move(-moving, 0.f);
                }

                else if (thoth::key_pressed(thoth::keyboard::right))
                {
                    m_player.sprite().animate("droite");
                    m_player.sprite().move(moving, 0.f);
                }
                else if (player_down == false && thoth::key_pressed(thoth::keyboard::space))
                {   m_player.m_time_frapper=0.8f;
                    if (m_player.sprite().animation().find("gauche") != std::string::npos)
                    {
                        m_player.sprite().animate("frapper_gauche");


                    }
                    else
                    {
                        m_player.sprite().animate("frapper_droite");
                    }
                    player_frappe = false;
                }

                else if (player_down == true)
                {
                    if (m_player.sprite().animation().find("gauche") != std::string::npos)
                    {
                        m_player.sprite().animate("bas_gauche");
                    }
                    else
                    {
                        m_player.sprite().animate("bas_droite");
                    }
                    m_player.sprite().move(0.f, moving);
                }
                else
                {
                     player_frappe = true;

                    if (m_player.sprite().animation().find("gauche") != std::string::npos)
                    {
                        m_player.sprite().animate("en_vie_gauche");
                    }
                    else
                    {
                        m_player.sprite().animate("en_vie_droite");

                    }


                }


            }



            if (m_player.m_time_frapper > 0.f && player_frappe)
            {

                if (m_player.sprite().animation().find("gauche") != std::string::npos)
                {
                    m_player.sprite().animate("frapper_gauche");
                    m_player.sprite().move(-moving, 0.f);

                }
                else
                {
                    m_player.sprite().animate("frapper_droite");
                      m_player.sprite().move(moving, 0.f);
                }
            }

            if (m_player.m_time_jump > 0.f)
            {
                if (m_player.sprite().animation().find("gauche") != std::string::npos)
                {
                    m_player.sprite().animate("saut_gauche");
                }
                else
                {
                    m_player.sprite().animate("saut_droite");
                }
                m_player.sprite().move(0.f, -3.5 * moving);


            }

			
//collison avec les personnage
            for(auto const & monstre : m_monstres)

            //Deplacement aleatoire des monstres
                        for (auto & monstre : m_monstres)
                        {
                            monstre.m_time_direction -= time_elapsed;

                            if (monstre.m_time_direction <= 0.f)
                            {
                                if (hopp::random::true_false())
                                {
                                    monstre.m_direction = direction_t::left;

                                }
                                else
                                {
                                    monstre.m_direction = direction_t::right;

                                }


                                monstre.m_time_direction = hopp::random::uniform(3.f, 5.f);
                            }
            //touché monstre
                        if (hopp::geometry::overlap(m_player.sprite().bounds_global(), monstre.sprite().bounds_global()))
                          {
                            if(monstre.m_direction == direction_t::left){




                                monstre.m_direction =  direction_t::right;

                                }
                            else
                                {


                                    monstre.m_direction = direction_t::left;

                                }

                          }

                        if (hopp::geometry::overlap(m_player.sprite().bounds_global(), monstre.sprite().bounds_global())){
                                player_touche = true;





                                    if(monstre.m_direction ==  direction_t::right)
                                        m_player.sprite().move(moving, -moving);

                                    if(monstre.m_direction ==  direction_t::left)

                                        m_player.sprite().move(moving, -moving);



                        }


                          monstre.move(moving);



                        }
        }
// 			for(auto const & plateforme : plateformes)
// 			{
// 				if (hopp::geometry::overlap(personnage.bounds_global(), plateforme.bounds_global()))
// 				{
// 					personnage.set_position(position_ex);
// 					break;
// 				}
// 			}
// 			
// 			for(auto const & monstre : monstres)
// 			{
// 				if (hopp::geometry::overlap(personnage.bounds_global(), monstre.bounds_global()))
// 				{
// 					personnage.animate("tourment");
// 					personnage.set_position(position_ex);
// 					break;
// 				}
// 			}

    }

	// Update
	void update(float const time_elapsed)
	{
		m_player.update(time_elapsed);
		
		for (auto & monstre : m_monstres)
		{
			monstre.update(time_elapsed);
		}
	}
};

// Affichage dans une fenêtre
inline thoth::window & operator <<(thoth::window & window, level_t const & level)
{
	for (auto const & decor : level.decors())
	{
		window << decor;
	}
	
	for (auto const & plateforme : level.plateformes())
	{
		window << plateforme;
	}
	
	for (auto const & monstre : level.monstres())
	{
		window << monstre;
	}
	
	window << level.player();
	
	return window;
}

#endif
